import React from 'react';
import { StyleSheet, Text, ScrollView, SafeAreaView, View, Button} from 'react-native';

export default function App() {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
            <Text style={{ fontSize: 24, paddingLeft: 16, paddingTop: 24}}>
                Compte
            </Text>
            <ScrollView>
                <View style={styles.signup}>
                    <Button
                        color="white"
                        title="Sign up"
                    />
                </View>

                <View style={styles.signin}>
                    <Button
                        color="black"
                        title="Sign in"
                    />
                </View>
                
            </ScrollView>
        </SafeAreaView>
    )
  }
  
  const styles = StyleSheet.create({
    signup: {
        marginHorizontal:16,
        marginTop:32,
        flex: 1,
        borderColor: '#000000',
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: '#000000'
    },
    signin: {
        marginHorizontal:16,
        marginVertical:8,
        flex: 1,
        borderColor: '#000000',
        borderWidth: 1,
        borderRadius: 8
    }
  });