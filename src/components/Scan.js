import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function App() {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    async () => {
      try {
        const value = await AsyncStorage.getItem('@storage_product');
        if (value !== null) {
          JSON.parse(value).append(data)
          try {
            // await AsyncStorage.setItem('products', JSON.stringify(value));
            await AsyncStorage.setItem('@storage_product', "1");
          } catch (error) { 
            console.log(error) 
          } 
        } else {
          try {
            await AsyncStorage.setItem('@storage_product', "1");
          } catch (error) { 
            console.log(error) 
          } 
        }
      } catch (err) {
        console.log(error) 
      }
    }
    setScanned(false);
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});



// return fetch('https://reactnative.dev/movies.json')
//         .then(response => response.json())
//         .then(responseJson => {
//             this.setState(
//                 {
//                     isLoading: false,
//                     dataSource: responseJson.movies,
//                 },
//                 function() {}
//             );
//         })
//         .catch(error => {
//             console.error(error);
//         });