import React from 'react';
import { StyleSheet, ActivityIndicator, FlatList, Text, ScrollView, SafeAreaView, View, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLoading: true, dataSource: [] };
    }

    componentWillMount() {
        this.getData()
    }

    getData = async () => {
        await AsyncStorage.getItem('@storage_product', (error, result) => {
            if (error !== null) { 
                this.setState(
                    {
                        isLoading: false
                    },
                    function () {   }
                );
            } else {
                this.setState(
                    { 
                        isLoading: false,
                        dataSource: result
                    }, 
                    function () {   }
                );
            }
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                    <Text style={{ fontSize: 24, paddingLeft: 16, paddingTop: 24}}>
                        Accueil
                    </Text>
                    <View style={{ flex: 1, paddingVertical: Dimensions.get('window').height / 2.5}}>
                        <ActivityIndicator />
                    </View>
                </SafeAreaView>
            );
        }

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: "white"}}>
                <Text style={{ fontSize: 24, paddingLeft: 16, paddingTop: 24}}>
                        Accueil
                </Text>

                <Text>Il y a {this.state.dataSource} </Text>
                <ScrollView>
                    <FlatList 
                        style={{paddingTop: 32}}
                        data={this.state.dataSource} renderItem={({ item }) => 
                        <View style={styles.item}>
                            <Text>{item.title}, {item.releaseYear}</Text> 
                        </View>
                        } 
                    />
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
    },
    item: {
        paddingVertical: 16,
        paddingHorizontal:16,
        borderColor: 'black',
        borderBottomWidth: 1,
        fontSize: 18,
        height: 50,
    },
});
