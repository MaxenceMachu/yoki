import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './src/components/Home'
import Account from './src/components/Account'
import Scan from './src/components/Scan'

const TabBarController = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <TabBarController.Navigator
        tabBarOptions={{
          activeTintColor: 'black',
        }}
      >
        <TabBarController.Screen 
          name="Accueil" 
          component={Home}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }} 
        />
        <TabBarController.Screen 
          name="Scanner" 
          component={Scan}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="camera" color={color} size={size} />
            ),
          }} 
        />
        <TabBarController.Screen 
          name="Compte" 
          component={Account} 
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="account" color={color} size={size} />
              ),
          }} 
        />
      </TabBarController.Navigator>
    </NavigationContainer>
  )
}